﻿using DispensaryDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace DispensaryDB.Tests
{
    public class ProfileTest
    {
        [Fact]
        public void GetFirstNameTest()
        {
            //Arrange
            var profile = new Profile();
            profile.FirstName = "Bhawani";
            //Act
            var result = profile.FirstName;

            //Assert
            Assert.Equal("Bhawani", result);
        }

    }
}
