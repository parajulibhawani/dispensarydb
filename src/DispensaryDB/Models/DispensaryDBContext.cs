﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
namespace DispensaryDB.Models
{
    public class DispensaryDBContext : IdentityDbContext<ApplicationUser>
    {
        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Dispensary> Dispensaries { get; set; }
        public virtual DbSet<Strain> Strains { get; set; }
        public virtual DbSet<Deal> Deals { get; set; }



        protected override void OnModelCreating(ModelBuilder builder)
        {

            base.OnModelCreating(builder);
            builder.Entity<Profile>()
        .Property(p => p.RowVersion).IsConcurrencyToken();
            builder.Entity<Strain>()
        .Property(p => p.RowVersion).IsConcurrencyToken();
            builder.Entity<Dispensary>()
        .Property(p => p.RowVersion).IsConcurrencyToken();
            builder.Entity<Deal>()
        .Property(p => p.RowVersion).IsConcurrencyToken();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=DispensaryDB;integrated security=True");
            
        }
    }
}
