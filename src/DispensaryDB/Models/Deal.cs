﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DispensaryDB.Models
{[Table("Deals")]
    public class Deal
    {[Key]
        public int DealId { get; set; }
        public string Title { get; set; }
        public Strain Strain { get; set; }

        public int Price { get; set; }

        public string Producer { get; set; }

        public string Image { get; set; }
        public int LocationId { get; set; }
        public int Unit { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}
