﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DispensaryDB.Models
{[Table("strains")]
    public class Strain
    {
        [Key]
        public int StrainId { get; set; }
        public string strain { get; set; }
        public string ProperName { get; set; }
        public string ScientificName { get; set; }
        public int THC { get; set; }
        public int CBD { get; set; }
        public int DispensaryId { get; set; }
        public string Image { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual ApplicationUser User { get; set; }

    }
}
