﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;

namespace DispensaryDB.Models
{   [Table("Profiles")]
    public class Profile
    {   
        [Key]       
        public int ProfileId { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public DateTime DOB { get; set; }
        
        public string Address { get; set; }
       
        public string City { get; set; }
        
        public string State { get; set; }
        
        public string Zip { get; set; }
        
        public string image { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        
        public virtual ApplicationUser User { get; set; }
    }
}
