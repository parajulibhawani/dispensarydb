﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DispensaryDB.Models
{   
    [Table("Dispensaries")]
    public class Dispensary
    {
        [Key]
        public int DispensaryId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string PhoneNumber { get; set; }
        public int LicenseNumber { get; set; }
        public DateTime? DOE { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public decimal Distance { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser User { get; set; }

        public JObject GetGeoCode() {
            var client = new RestClient("https://maps.googleapis.com/maps/api/geocode/json?address=" + Address + ',' + Zip + "&key="+EnvironmentVariables.googleApiKey);
            var request = new RestRequest(Method.GET);
            var response = new RestResponse();
            Task.Run(async () =>
            {
                response = await GetResponseContentAsync(client, request) as RestResponse;
            }).Wait();
            //4
            //Console.WriteLine(response.Content);
            JObject jsonResponse = JsonConvert.DeserializeObject<JObject>(response.Content);
            //Console.WriteLine(jsonResponse.SelectToken("results[0].geometry.location.lat"));
            return jsonResponse;   
        }

        public static Task<IRestResponse> GetResponseContentAsync(RestClient theClient, RestRequest theRequest)
        {
            var tcs = new TaskCompletionSource<IRestResponse>();
            theClient.ExecuteAsync(theRequest, response => {
                tcs.SetResult(response);
            });
            return tcs.Task;
        }

    }
}
