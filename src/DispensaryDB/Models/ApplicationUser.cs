﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using RestSharp;
using RestSharp.Authenticators;

namespace DispensaryDB.Models
{
    public class ApplicationUser : IdentityUser
    {
        public bool MailingList { get; set; }
        public void AddToMailingList()
        {
            var client = new RestClient("https://us14.api.mailchimp.com/3.0");
            var request = new RestRequest("lists/" + EnvironmentVariables.listId + "/members", Method.POST);
            request.AddJsonBody(new { status = "subscribed", email_address = Email });
            client.Authenticator = new HttpBasicAuthenticator("parajulibhawani", EnvironmentVariables.apiKey);
            client.ExecuteAsync(request, response =>
            {
                Console.WriteLine(response.Content);
            });
        }

    }
}
