﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispensaryDB.Models
{
    [Table("Locations")]
    public class Location
    {
        [Key]
        public int LocationId { get; set; }
        public string City { get; set; }
        public  string Zip { get; set; }

        public string State { get; set; }

        public string Country { get; set; }

        //public decimal Latitude { get; set; }
        //public decimal Longitude { get; set; }
        public virtual ICollection<Dispensary> Dispensaries { get; set; }
        public virtual ICollection<Deal> Deals { get; set; }

    }
}
