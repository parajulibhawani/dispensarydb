﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispensaryDB.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace DispensaryDB.Controllers
{
    [Route("api/dispensaries")]
    public class DispensaryController : Controller
    {
        private readonly DispensaryDBContext _db;
        public DispensaryController(DispensaryDBContext db)
        {
            _db = db;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            var thisList = _db.Dispensaries.ToList();
             return Json(thisList);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
