﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using DispensaryDB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Security.Claims;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Net.Http.Headers;
using Microsoft.EntityFrameworkCore;



// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace DispensaryDB.Controllers
{
    [Authorize]
    public class ProfilesController : Controller
    {

        private readonly DispensaryDBContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private IHostingEnvironment _env;
        public ProfilesController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, RoleManager<IdentityRole> roleManager, DispensaryDBContext db, IHostingEnvironment env)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _db = db;
            _env = env;
        }


 
        public async Task<IActionResult> Index()
        {
            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var currentUser = await _userManager.FindByIdAsync(userId);
            
            
            return View(_db.Profiles.Where(x => x.User.Id == currentUser.Id));
        }
        public IActionResult Create()
        {
            return View();
        }
        [Authorize(Roles ="User")]
        [HttpPost]
        public async Task<IActionResult> Create(Profile profile, IFormFile image)
        {
            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var currentUser = await _userManager.FindByIdAsync(userId);
            profile.User = currentUser;
            
            var fileName = ContentDispositionHeaderValue
                .Parse(image.ContentDisposition)

                .FileName

                .Trim('"');
           
            profile.image = fileName;

            _db.Profiles.Add(profile);
            _db.SaveChanges(); if (image.Length > 0 )
            {

                var uploads = Path.Combine(_env.WebRootPath, "img");

                using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                {
                    await image.CopyToAsync(fileStream);

                }

            }
            return RedirectToAction("Index");
        }
        public IActionResult Edit(int id)
        {
            var profile = _db.Profiles.FirstOrDefault(p => p.ProfileId == id);
            return View(profile);
        }
        [Authorize(Roles ="User")]
        [HttpPost]
        public async Task<IActionResult> Edit(int ? id, [Bind(include:" ProfileId, FirstName , LastName, DOB, Address, City, State, Zip, image, RowVersion")] Profile profile, IFormFile image)
        {
            
            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var currentUser = await _userManager.FindByIdAsync(userId);
            
        
            if (image!=null)
            {
                if (profile.image.Any())
                {
                    _db.Profiles.Remove(profile);

                }
                
                var fileName = ContentDispositionHeaderValue
              .Parse(image.ContentDisposition)

              .FileName

              .Trim('"');

                var uploads = Path.Combine(_env.WebRootPath, "img");

                using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                {
                    await image.CopyToAsync(fileStream);
                    profile.image = fileName;

                }

            }
            _db.Entry(profile).State = EntityState.Modified;
            _db.SaveChanges();
            ModelState.Clear();
           
            return RedirectToAction("Index");
        }
    }
}


