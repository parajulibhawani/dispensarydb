﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispensaryDB.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;



// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace DispensaryDB.Controllers
{
    
    public class DispensariesController : Controller
    {
        private readonly DispensaryDBContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        public DispensariesController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, RoleManager<IdentityRole> roleManager, DispensaryDBContext db)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _db = db;

        }
        public async Task<IActionResult> Index()
        {

            var thisList = _db.Dispensaries.ToList();
            ViewBag.list = thisList;
            return View(thisList);
        }
        public IActionResult DispensaryNav()
        {
            return View();
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Dispensary dispensary)
        {
            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var currentUser = await _userManager.FindByIdAsync(userId);
            dispensary.User = currentUser;
            var geocode = dispensary.GetGeoCode();
            dispensary.Address = geocode.SelectToken("results[0].formatted_address").ToString();
            var addressArray = dispensary.Address.Split(',');
            dispensary.City = addressArray[1].ToString();
            var splitZipState = addressArray[2].Split(' ');
            dispensary.State = splitZipState[1].ToString();
            dispensary.Zip = splitZipState[2].ToString();
            dispensary.Latitude = Convert.ToDecimal(geocode.SelectToken("results[0].geometry.location.lat").ToString());
            dispensary.Longitude = Convert.ToDecimal(geocode.SelectToken("results[0].geometry.location.lng").ToString());
            _db.Dispensaries.Add(dispensary);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }
        public IActionResult Login()
        {
            return RedirectToAction("Login","Home");
        }
    }
}

