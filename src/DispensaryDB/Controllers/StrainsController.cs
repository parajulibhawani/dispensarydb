﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using DispensaryDB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Security.Claims;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Net.Http.Headers;
using Microsoft.EntityFrameworkCore;



// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace DispensaryDB.Controllers
{
    [Authorize]
    public class StrainsController : Controller
    {

        private readonly DispensaryDBContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private IHostingEnvironment _env;
        public StrainsController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, RoleManager<IdentityRole> roleManager, DispensaryDBContext db, IHostingEnvironment env)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _db = db;
            _env = env;
        }


        public async Task<IActionResult> Index()
        {
            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var currentUser = await _userManager.FindByIdAsync(userId);

            return View(_db.Strains.Where(x => x.User.Id == currentUser.Id));
        }
        public IActionResult Create()
        {
            return View();
        }
        [Authorize(Roles = "User")]
        [HttpPost]
        public async Task<IActionResult> Create(Strain newStrain, IFormFile Image)
        {
            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var currentUser = await _userManager.FindByIdAsync(userId);
            newStrain.User = currentUser;
            var fileName = ContentDispositionHeaderValue
                .Parse(Image.ContentDisposition)

                .FileName

                .Trim('"');
            
            newStrain.Image = fileName;
            _db.Strains.Add(newStrain);
            _db.SaveChanges(); if (Image.Length > 0)
            {

                var uploads = Path.Combine(_env.WebRootPath, "img");

                using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                {
                    await Image.CopyToAsync(fileStream);

                }

            }
            return RedirectToAction("Index");
        }
        public IActionResult Edit(int id)
        {
            var strain = _db.Strains.FirstOrDefault(p => p.StrainId == id);
            return View(strain);
        }
        [Authorize(Roles = "User")]
        [HttpPost]
        public async Task<IActionResult> Edit(int? id, [Bind(include: " StrainId, ProperName , Strain, ScientificName, THC, CBD, RetailPrice, Effects, image, RowVersion")] Strain newStrain, IFormFile Image)
        {

            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var currentUser = await _userManager.FindByIdAsync(userId);
           
            
            if (Image != null)
            {
                if (newStrain.Image.Any())
                {
                    _db.Strains.Remove(newStrain);

                }
                var fileName = ContentDispositionHeaderValue
              .Parse(Image.ContentDisposition)

              .FileName

              .Trim('"');

                var uploads = Path.Combine(_env.WebRootPath, "img");

                using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                {
                    await Image.CopyToAsync(fileStream);
                    newStrain.Image = fileName;
                }

            }
            _db.Entry(newStrain).State = EntityState.Modified;
            _db.SaveChanges();
            ModelState.Clear();

            return RedirectToAction("Index");
        }
    }
}


