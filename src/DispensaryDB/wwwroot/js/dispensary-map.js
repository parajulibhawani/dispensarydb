﻿//request for dispensary info
var request = new XMLHttpRequest();
function NearbyObject(destinationAddresses, distance ) {
    this.destinationAddresses = destinationAddresses;
    this.distance = distance;
}

var dispensariesJson;
var dispensaries = [];
request.onreadystatechange = function () {
    if (request.readyState === 4 && request.status === 200) {
        dispensariesJson = JSON.parse(request.response);
        var nearbyList = [];

        for (var i = 0; i < dispensariesJson.length; i++) {
            var dispensaryId = dispensariesJson[i].dispensaryId;
            var name = dispensariesJson[i].name;
            var address = dispensariesJson[i].address;
            var city = dispensariesJson[i].city;
            var state = dispensariesJson.state;
            var zip = dispensariesJson[i].zip;
            var phoneNumber = dispensariesJson[i].phoneNumber;
            var website = dispensariesJson[i].website;
            var doe = dispensariesJson[i].doe;
            var licenseNumber = dispensariesJson[i].licenceNumber;
            var latitude = dispensariesJson[i].latitude;
            var longitude = dispensariesJson[i].longitude;
            var user = dispensariesJson[i].user;
            var distance = dispensariesJson[i].distance;
            var dispensary = new Dispensary(name, address, city, state, zip, phoneNumber, website, doe, licenseNumber, latitude, longitude, user, distance, dispensaryId);
            dispensaries.push(dispensary);
        }

    }
    else {
        throw new Error("Not found");

    }
    var destination = [];
    var dis;

    for (var x = 0; x < dispensaries.length; x++) {

        var address = dispensaries[x].address;
        var lat = parseFloat(dispensaries[x].latitude);
        var lng = parseFloat(dispensaries[x].longitude);
        var latlng = { latitude, longitude };
        var dispensaryLatLng = { lat: lat, lng: lng };
       var  geocoder = new google.maps.Geocoder();

       if (dispensaries[x].distance === 0) {
           console.log(lat);

           geocoder.geocode({ 'location': dispensaryLatLng }, function (results, status) {


               if (status === google.maps.GeocoderStatus.OK) {
                   map.setCenter(dispensaryLatLng);
                   destination.push(results[0].formatted_address);
                   var nearbyObject = new NearbyObject();

                   var origin = new google.maps.LatLng(origin1.lat, origin1.lng);
                   var destination1 = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());

                   var service = new google.maps.DistanceMatrixService();
                   service.getDistanceMatrix({
                       origins: [origin],
                       destinations: [destination1],
                       unitSystem: google.maps.UnitSystem.IMPERIAL,
                       travelMode: 'DRIVING'

                   }, callback);

                   function callback(response, status) {

                       if (status == 'OK') {

                           var origins = response.originAddresses;
                           var destinationAddresses = response.destinationAddresses;
                           nearbyObject.destinationAddresses = destinationAddresses;

                           for (var i = 0; i < origins.length; i++) {

                               var results = response.rows[i].elements;
                               for (var j = 0; j < results.length; j++) {
                                   var element = results[j];
                                   dis = element.distance.text;

                                   var duration = element.duration.text;
                                   var from = origins[i];
                                   var to = destinationAddresses[j];

                               }


                           }
                           nearbyObject.distance = dis;

                           nearbyList.push(nearbyObject);


                       }


                     


                   }
                   


               }

           });

        }

       
       
       
    }
   
   
}

request.open('GET', 'http://localhost:5000/api/dispensaries');
request.send();
