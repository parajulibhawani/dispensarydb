﻿//dispensary object
function Dispensary(name, address, city, state, zip, phoneNumber, website, licenceNumber, doe, latitude, longitude, user, distance) {
    this.name = name;
    this.address = address;
    this.city = city;
    this.state = state;
    this.zip = zip;
    this.phoneNumber = phoneNumber;
    this.website = website;
    this.licenseNumber = licenceNumber;
    this.doe = doe;
    this.latitude = latitude;
    this.longitude = longitude;
    this.user = user;
    this.distance = distance;
}

