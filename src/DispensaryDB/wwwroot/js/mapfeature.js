﻿/// <reference path="dispensaries.js" />
/// <reference path="dispensaries.js" />
/// <reference path="dispensaries.js" />
var map;
var origin1;
function myMap() {
    var mapCanvas = document.getElementById("map");
    var mapOptions = {
        center: new google.maps.LatLng(17.4018914, 78.5167823),
        zoom: 13
    }
    map = new google.maps.Map(mapCanvas, mapOptions);
    var infoWindow = new google.maps.InfoWindow({ map: map });
    // Try HTML5 geolocation.
    
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');

            map.setCenter(pos);
            origin1 = pos;

        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                                'Error: The Geolocation service failed.' :
                                'Error: Your browser doesn\'t support geolocation.');
    }

  
    }
 