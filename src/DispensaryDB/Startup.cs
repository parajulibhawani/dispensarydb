﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore;
using DispensaryDB.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http.Features;

namespace DispensaryDB
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; set; }
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        public async void DataInitialize()
        {
            DispensaryDBContext db = new DispensaryDBContext();
            string[] roles = new string[] { "Admin", "Moderator", "User" };
            var roleStore = new RoleStore<IdentityRole>(db);
            foreach(string role in roles)
            {
                if(!db.Roles.Any(r=>r.Name == role))
                {
                    IdentityRole newRole = new IdentityRole(role);
                    newRole.NormalizedName = role.ToUpper();
                    await roleStore.CreateAsync(newRole);
                }
            }
            var userStore = new UserStore<ApplicationUser>(db);
            if(!db.Users.Any(u=>u.Email == "parajulibhawani@gmail.com"))
            {
                var user = new ApplicationUser { Email = "parajulibhawani@gmail.com", UserName = "parajulibhawani" };
                var password = new PasswordHasher<ApplicationUser>();
                var hashed = password.HashPassword(user, EnvironmentVariables.adminPass);
                user.PasswordHash = hashed;
                user.NormalizedEmail = user.Email.ToUpper();
                user.SecurityStamp = Guid.NewGuid().ToString();
                user.NormalizedUserName = user.UserName.ToUpper();
                await userStore.CreateAsync(user);
                await userStore.AddToRoleAsync(user, "ADMIN");
            }
            await db.SaveChangesAsync();
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            
            
            services.AddMvc()
                .AddXmlSerializerFormatters();
            services.Configure<FormOptions>(options =>
            {

                options.MultipartBodyLengthLimit = 60000000;
            });
            services.AddEntityFramework()
                .AddDbContext<DispensaryDBContext>(options =>
                    options.UseSqlServer(Configuration["ConnectionStrings:DefaultConnection"]));
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<DispensaryDBContext>()
                .AddDefaultTokenProviders();
           
        }


        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();
            app.UseIdentity();
            app.UseStaticFiles(
                );
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
               
            });
            
            DataInitialize();
            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Hello World!");
            });
        }
   
    }
}
