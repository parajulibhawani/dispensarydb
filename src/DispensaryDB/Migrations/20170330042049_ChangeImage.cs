﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DispensaryDB.Migrations
{
    public partial class ChangeImage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Image",
                table: "strains",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "image",
                table: "Profiles",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Image",
                table: "Deals",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte[]>(
                name: "Image",
                table: "strains",
                nullable: true);

            migrationBuilder.AlterColumn<byte[]>(
                name: "image",
                table: "Profiles",
                nullable: true);

            migrationBuilder.AlterColumn<byte[]>(
                name: "Image",
                table: "Deals",
                nullable: true);
        }
    }
}
