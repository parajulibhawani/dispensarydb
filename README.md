###DispensaryDB

##By Bhawani Parajuli, 09/2016

#Description
This website shows the dispensaries nearby the user's location. An individual user can use it to view the dispensaries available in our database as a list or in the map. There is also option for the user to create a page for their business if they can't find it in the website. Since, this web app is in development phase it's not available for browsing as of today. Live version coming soon. If you are interested and have required environments setup please follow the setup instruction.

#Technology used
Visual Studio 2015 Update 3, NetCore,
Entity, C#, Javascript, AJAX, SQL Server Management Studio, Github, MailChimp API

#Setup(For Windows)
Pre-requisites:
* Download Visual Studio 2015 Update 3
* Download SQL Server Management Studio
* MailChimp API key and listid (MailChimp provides it when you sign up for free account.)


After Setup:
* git clone https://github.com/parajulibhawani/DispensaryDB
* Open the project on visual studio
* Right click solution explorer and "Build"
* On command line find the folder that contains project.json, enter:
dotnet ef database update [Name of the last migration in the Migrations folder]
* Click the green play button to serve in the explorer.

## License

MIT License

Copyright (c) 2016 Bhawani Parajuli

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
